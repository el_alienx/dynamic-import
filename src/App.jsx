// Core
import React from "react";

// Components
import Card from "./Card";

// Othe imports
// We write a json file for each of our categories
// or we use a 1 single json and then use a filter
// function to have their data separated.
import cupcakes from "./data-cupcake.json";
import weddings from "./data-weddings.json";
import "./style.css";

export default function App() {
  // Components
  const Product1 = cupcakes.map((item) => <Card key={item.id} data={item} />);
  const Product2 = weddings.map((item) => <Card key={item.id} data={item} />);

  return (
    <div className="App">
      <h1>Products</h1>

      {/* Cupckaes */}
      <section>
        <h2>Cupckaes</h2>
        <div className="grid">{Product1}</div>
      </section>

      {/* Wedding cakes */}
      <section>
        <h2>Wedding cakes</h2>
        <div className="grid">{Product2}</div>
      </section>
    </div>
  );
}
