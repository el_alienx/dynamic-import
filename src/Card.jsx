// Note 1:
// The reason we need a Promise is because by default import statements are outside the component
// and they take care of loading the assets before the component is rendered.
// But inside the component we are already showing stuff on the screen, so we need the promise to
// know when we have the data ready to be use, in this case the image downloaded.

// Note 2:
// In the json we store the folder and the filename of the picture instead of the full path.
// The reason is that the json should only know the basic of organizing the photos (one folder for
// cupcake, other folder for wedding cakes) but is the import statement that should tell React where
// is is located this images folder, that contain other folders.

// Note 3:
// Get used to rename your jpg files. Sometimes they will have a weird "e" in the extension .jpeg
// instead of .jpg and that will cause problems, so rename the file and it will be fixed.

// Core
import React, { useEffect, useState } from "react";

export default function Card({ data }) {
  const { file_name, name, price } = data;

  // We are going to use this hook to hold the image
  const [imageURL, setImageURL] = useState("");

  // We use the useEffect hook to dynamically import images
  useEffect(() => {
    // We add a Promise (.then methdod) to do the import
    import(`./images/${file_name}`).then((image) => {
      // When is ready we store the returning
      // image inside the .default param
      setImageURL(image.default);
    });
  });

  return (
    <article className="card">
      {/* We use the hook to render the image */}
      <img
        alt="A specific picture of the cupcake"
        className="image"
        src={imageURL}
        title={name}
      />
      <h3 className="title">{name}</h3>
      <p className="price">{price}</p>
    </article>
  );
}
